FROM ruby:2.7-buster

WORKDIR /lifegame_ruby

ARG BUNDLE_INSTALL_ARGS="-j 4"
COPY Gemfile* ./
RUN bundle config --local disable_platform_warnings true && bundle install ${BUNDLE_INSTALL_ARGS}

COPY . ./

CMD ["bundle", "exec", "ruby", "app.rb"]
