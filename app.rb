# coding: utf-8
require 'sinatra'
require 'sinatra/reloader'
include ERB::Util

set :bind, '0.0.0.0'

get '/lifegame' do
  if request['prev']
    b = []
    request['prev'].split('9').each do |ln|
      r = []
      ln.split('').each do |c|
        if c == '0'
          r << '□'
        else
          r << '■'
        end
      end
      b << r
    end
    bb = []
    for i in 0..4 do
      bb << []
      for _ in 0..4 do
        bb[i] << '□'
      end
    end
    for i in 0..4 do
      for j in 0..4 do
        count = 0
        if b[i][j] == '□'
          # 誕生の場合
          count = 0
          if b[i-1][j-1] == '■'; count += 1 end
          if b[i-1][j] == '■'; count += 1 end
          if b[i-1][j+1] == '■'; count += 1 end
          if b[i][j-1] == '■'; count += 1 end
          if b[i][j+1] == '■'; count += 1 end
          if i < 4 && b[i+1][j-1] == '■'; count += 1 end
          if i < 4 && b[i+1][j] == '■'; count += 1 end
          if i < 4 && b[i+1][j+1] == '■'; count += 1 end
          if count >= 3
            bb[i][j] = '■'
          else
            bb[i][j] = '□'
          end
        end
        if b[i][j] == '■'
          # 生存・過疎・過密の場合
          count = 0
          if b[i-1][j] == '■'; count += 1 end
          if b[i][j-1] == '■'; count += 1 end
          if b[i][j+1] == '■'; count += 1 end
          if b[i+1][j] == '■'; count += 1 end
          if count == 2
            bb[i][j] = b[i][j]
          else
            bb[i][j] = '□'
          end
        end
      end
    end
    n = []
    bb.each do |r|
      nr = []
      r.each do |c|
        if c == '□'
          nr.push('0');
        else
          nr.push('1');
        end
      end
      n << nr.join('')
    end
    tstp = Time.now().iso8601()
    erb :lifegame, locals: { b: bb, s: n.join('9'), tstp: tstp }
  else
    b = []
    for i in 0..4 do
      b << []
      for _ in 0..4 do
        b[i] << '□'
      end
    end
    b[2][1] = '■'
    b[2][2] = '■'
    b[2][3] = '■'
    n = []
    b.each do |r|
      nr = []
      r.each do |c|
        if c == '□'
          nr.push('0');
        else
          nr.push('1');
        end
      end
      n << nr.join('')
    end
    tstp = Time.now().iso8601()
    erb :lifegame, locals: { b: b, s: n.join('9'), tstp: tstp }
  end
end
